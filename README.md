# IP-localisation

## what it is?

This is an application for IP address geolocation on Leaflet maps by using API of ipapi.co and JavaScript/HTML/LESS

IP Geolocation est un projet de géolocalisation d'adresses IP développé en utilisant l'API de ipapi.co, JavaScript/HTML/LESS et Leaflet. Il permet à l'utilisateur de trouver l'emplacement de son adresse IP sur une carte interactive en utilisant les bibliothèques de cartographie Leaflet. Le projet utilise également l'API de ipapi.co pour obtenir des informations sur l'emplacement de l'adresse IP, y compris le pays, la région et la ville et etc...

![view](assets/v.png)
![view](assets/v2.png)
